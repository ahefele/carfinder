//
//  UIColor+TCColor.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/23/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TCColor)
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
