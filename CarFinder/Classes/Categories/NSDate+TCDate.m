//
//  NSDate+TCDate.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/25/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "NSDate+TCDate.h"

@implementation NSDate (TCDate)

+(NSInteger)currentYear
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    return [components year];
}

@end
