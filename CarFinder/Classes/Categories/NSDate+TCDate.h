//
//  NSDate+TCDate.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/25/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TCDate)

+(NSInteger)currentYear;

@end
