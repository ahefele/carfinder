//
//  Utility.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/24/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+(void)copyBundleFile:(NSString*)fileName;
+(NSString*)getDBPathAsStringForDB:(NSString*)dbName;

@end
