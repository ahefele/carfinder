//
//  Cars.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CarsDelegate;

@interface Cars : NSObject

@property (nonatomic, strong) NSMutableArray* carList;
@property (nonatomic, weak) id<CarsDelegate> delegate;
@property (nonatomic) NSInteger selectedYear;
@property (nonatomic, strong) NSString* selectedMake;

-(void)clearCarList;
-(void)deleteCarAtIndex:(NSInteger)carIndex;

-(NSArray*)getAvailableYears;
-(NSArray*)getAvailableMakes;
-(void)getAvailableCars;

@end

@protocol CarsDelegate <NSObject>

@optional
-(void)carModelDataLoadCompleted:(Cars*)carModel;

@end
