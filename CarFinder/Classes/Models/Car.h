//
//  Car.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface Car : NSObject

@property (nonatomic) NSInteger carId;
@property (nonatomic) NSInteger year;
@property (nonatomic, strong) NSString* make;
@property (nonatomic, strong) NSString* model;
@property (nonatomic, strong) NSMutableArray* modelErrors;

-(void)populateFromResults:(FMResultSet*)results;
-(void)reset;
-(BOOL)isValidModel;
-(BOOL)saveModel;
-(void)deleteModel;

@end
