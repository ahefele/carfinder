//
//  Cars.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "Cars.h"
#import "Car.h"
#import "Utility.h"
@implementation Cars
{
    NSMutableArray* _availableYears;
    NSMutableArray* _availableMakes;
    
    BOOL _isYearCacheExpired;
    BOOL _isMakeCacheExpired;
}

#pragma mark - Properties
-(void)setSelectedYear:(NSInteger)value
{
    if(_selectedYear != value)
    {
        _selectedMake = nil;
        [_carList removeAllObjects];
    }
    _selectedYear = value;
}

-(void)setSelectedMake:(NSString *)value
{
    _selectedMake = value;
    [self getAvailableCars];
}

-(id)init
{
    self = [super init];
    if(self)
    {
        _availableYears = [[NSMutableArray alloc]init];
        _availableMakes = [[NSMutableArray alloc]init];
        _carList = [[NSMutableArray alloc]init];
        _isYearCacheExpired = YES;
        _isMakeCacheExpired = YES;
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(carYearCacheExpired:) name:@"carYearCacheExpired" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(carModelCacheExpired:) name:@"carModelCacheExpired" object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - Notifications
-(void)carYearCacheExpired:(NSNotification*)notification
{
    _isYearCacheExpired = YES;
    [self carModelCacheExpired:notification];
}

-(void)carModelCacheExpired:(NSNotification*)notification
{
    _isMakeCacheExpired = YES;
}

-(void)clearCarList
{
    [_carList removeAllObjects];
}

-(void)deleteCarAtIndex:(NSInteger)carIndex
{
    Car* selectedCar = [self.carList objectAtIndex:carIndex];
    [selectedCar deleteModel];
    [self.carList removeObjectAtIndex:carIndex];
}

-(NSArray*)getAvailableYears
{
    if(_isYearCacheExpired)
    {
        [_availableYears removeAllObjects];
        
        FMDatabase* fmdb = [FMDatabase databaseWithPath:[Utility getDBPathAsStringForDB:@"Cars.db"]];
        if([fmdb open])
        {
            FMResultSet* results = [fmdb executeQuery:@"SELECT DISTINCT Year FROM Car ORDER BY Year"];
            if(results)
            {
                while([results next])
                {
                    [_availableYears addObject:[results stringForColumn:@"Year"]];
                }
                [results close];
            }
            [fmdb close];
        }
        _isYearCacheExpired = NO;
    }
    return _availableYears;
}

-(NSArray*)getAvailableMakes
{
    if(_isMakeCacheExpired)
    {
        [_availableMakes removeAllObjects];
        
        FMDatabase* fmdb = [FMDatabase databaseWithPath:[Utility getDBPathAsStringForDB:@"Cars.db"]];
        if([fmdb open])
        {
            FMResultSet* results = [fmdb executeQuery:@"SELECT DISTINCT Make FROM Car WHERE Year = ? ORDER BY Make", [NSNumber numberWithInteger:self.selectedYear]];
            if(results)
            {
                while([results next])
                {
                    [_availableMakes addObject:[results stringForColumn:@"Make"]];
                }
                [results close];
            }
            [fmdb close];
        }
        _isMakeCacheExpired = NO;
    }
    
    return _availableMakes;
}

-(void)getAvailableCars
{
    __weak __typeof__(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.carList removeAllObjects];
        FMDatabase* fmdb = [FMDatabase databaseWithPath:[Utility getDBPathAsStringForDB:@"Cars.db"]];
        if([fmdb open])
        {
            FMResultSet* results = [fmdb executeQuery:@"SELECT rowid, Year, Make, Model FROM Car WHERE Year = ? AND Make= ? ORDER BY Year, Make, Model", [NSNumber numberWithInteger:self.selectedYear], self.selectedMake];
            if(results)
            {
                while([results next])
                {
                    Car* car = [[Car alloc]init];
                    [car populateFromResults:results];
                    [weakSelf.carList addObject:car];
                }
                [results close];
            }
            [fmdb close];
        }
        id<CarsDelegate> strongDelegate = weakSelf.delegate;
        
        if([strongDelegate respondsToSelector:@selector(carModelDataLoadCompleted:)])
        {
            [strongDelegate carModelDataLoadCompleted:self];
        }
    });
}

@end