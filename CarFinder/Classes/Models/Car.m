//
//  Car.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "Car.h"
#import "FMDatabaseQueue.h"
#import "Utility.h"

@implementation Car
{
    BOOL _isNew;
}
-(id)init
{
    self = [super init];
    if(self)
    {
        _carId = 0;
        _year = [NSDate currentYear];
        _make = @"";
        _model = @"";
        _modelErrors = [[NSMutableArray alloc]init];
        _isNew = YES;
    }
    
    return self;
}

-(void)populateFromResults:(FMResultSet*)results
{
    self.carId = [results intForColumn:@"rowid"];
    self.year = [results intForColumn:@"Year"];
    self.make = [results stringForColumn:@"Make"];
    self.model = [results stringForColumn:@"Model"];
}

-(void)reset
{
    self.carId = 0;
    self.year = [NSDate currentYear];
    self.make = @"";
    self.model = @"";
}

-(BOOL)isValidModel
{
    BOOL isValid = YES;
    
    [self.modelErrors removeAllObjects];
    NSInteger maxYear = [NSDate currentYear] + 5;
    
    if((self.year < 1901) || (self.year > maxYear))
    {
        isValid = NO;
        [self.modelErrors addObject:[NSString stringWithFormat:@"Year must be between 1901 and %ld", maxYear]];
    }
    
    if(self.make.length == 0)
    {
        isValid = NO;
        [self.modelErrors addObject:@"Enter a car make"];
    }
    
    if(self.model.length == 0)
    {
        isValid = NO;
        [self.modelErrors addObject:@"Enter a car model"];
    }
    
    return isValid;
}

-(BOOL)saveModel
{
    BOOL wasSuccessful = NO;
    
    if([self isValidModel])
    {
        if(self.carId == 0)
        {
            [currentAppDelegate.dbQueue inDatabase:^(FMDatabase *db)
             {
                 [db executeUpdate:@"INSERT INTO Car(Year, Make, Model) VALUES(?,?,?)",
                  [NSNumber numberWithInteger:self.year],
                  self.make,
                  self.model];
             }];
        }
        else
        {
            [currentAppDelegate.dbQueue inDatabase:^(FMDatabase *db)
             {
                 [db executeUpdate:@"UPDATE Car SET Year = ?, Make = ?, Model = ? WHERE rowid = ?",
                  [NSNumber numberWithInteger:self.year],
                  self.make,
                  self.model,
                  [NSNumber numberWithInteger:self.carId]];
             }];
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:@"carYearCacheExpired" object:self];
        wasSuccessful = YES;
    }
    
    return wasSuccessful;
}

-(void)deleteModel
{
    [currentAppDelegate.dbQueue inDatabase:^(FMDatabase *db)
     {
         db.logsErrors = YES;
         db.traceExecution = YES;
         [db executeUpdate:@"DELETE FROM Car WHERE rowid = ?",
          [NSNumber numberWithInteger:self.carId]];
     }];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"carYearCacheExpired" object:self];
}

@end