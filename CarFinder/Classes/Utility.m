//
//  Utility.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/24/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(void)copyBundleFile:(NSString*)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if ([fileManager fileExistsAtPath:txtPath] == NO)
    {
        NSArray* fileNameParts = [fileName componentsSeparatedByString:@"."];
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:fileNameParts[0] ofType:fileNameParts[1]];
        [fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error];
        
        if(error)
        {
            NSLog(@"Failed to copy bundle file: %@: %@", fileName, error.localizedDescription);
        }
    }
}

+(NSString*)getDBPathAsStringForDB:(NSString*)dbName
{
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docsDir = [dirPaths objectAtIndex:0];
    NSString* databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:dbName]];
    
    return databasePath;
}

@end
