//
//  GenericSelectionViewController.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "TCViewController.h"

@interface GenericSelectionViewController : TCViewController

@property (nonatomic, weak) IBOutlet UITableView* dataTable;
@property (nonatomic, weak) IBOutlet UISearchBar* tableSearchBar;

@property (nonatomic, strong) NSString* viewTitle;
@property (nonatomic, strong) NSString* tableHeading;
@property (nonatomic, weak) NSString* selectedNotificationName;

@property (nonatomic, weak) NSArray* dataArray;
@property (nonatomic) NSString* selectedValue;
@property (nonatomic) BOOL readOnly;

@end
