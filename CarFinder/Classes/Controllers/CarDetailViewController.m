//
//  CarDetailViewController.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/24/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "CarDetailViewController.h"

@interface CarDetailViewController ()
{
    
}
@end

@implementation CarDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - View Events
-(IBAction)saveClicked:(id)sender
{
    [self resignFirstResponder];
    self.carModel.year = [self.yearField.text integerValue];
    self.carModel.make = self.makeField.text;
    self.carModel.model = self.modelField.text;
    
    if([self.carModel isValidModel])
    {
        [self.carModel saveModel];
        [self performSegueWithIdentifier:@"unwindToCarListViewSegue" sender:self];
    }
    else
    {
        NSMutableString* errorMessage = [[NSMutableString alloc]init];
        [errorMessage appendString:@"Correct the following: \n\n"];
        for(NSString* error in self.carModel.modelErrors)
        {
            [errorMessage appendFormat:@"%@\n", error];
        }
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorMessage delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
        [alert show];
    }
}

-(IBAction)backgroundTap:(id)sender
{
    [self resignAllFirstResponders];
}

#pragma mark - General
-(void)setupView
{
    [super setupView];
    
    self.yearField.text = [NSString stringWithFormat:@"%ld", (long)self.carModel.year];
    self.makeField.text = self.carModel.make;
    self.modelField.text = self.carModel.model;
    
    self.navigationItem.title = (self.carModel.carId > 0) ? @"Edit Car" : @"Add Car";
}

-(void)resignAllFirstResponders
{
    [self.yearField resignFirstResponder];
    [self.makeField resignFirstResponder];
    [self.modelField resignFirstResponder];
}

@end