//
//  TCViewController.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCViewController : UIViewController

-(void)showGenericSelectionViewWithTitle:(NSString*)title tableHeading:(NSString*)tableHeading dataArray:(NSArray*)dataArray selectedValue:(NSString*)selectedValue isReadOnly:(BOOL)readOnly selectedNotificationName:(NSString*)selectedNotificationName;

-(void)setupView;
@end
