//
//  CarDetailViewController.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/24/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"

@interface CarDetailViewController : TCViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField* yearField;
@property (nonatomic, weak) IBOutlet UITextField* makeField;
@property (nonatomic, weak) IBOutlet UITextField* modelField;

@property (nonatomic, weak) Car* carModel;

@end
