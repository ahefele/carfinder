//
//  CarListViewController.h
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cars.h"

@interface CarListViewController : TCViewController <UITableViewDataSource, UITableViewDelegate, CarsDelegate>

@property (nonatomic, weak) IBOutlet UIButton* yearButton;
@property (nonatomic, weak) IBOutlet UIButton* makeButton;
@property (nonatomic, weak) IBOutlet UITableView* carListTable;

@end
