//
//  GenericSelectionViewController.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "GenericSelectionViewController.h"
@interface GenericSelectionViewController ()
{
    NSMutableArray* _filteredTableData;
}
@end

@implementation GenericSelectionViewController

typedef void(^myCompletion)(BOOL);

#pragma mark - Lifecycle
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UITapGestureRecognizer *_tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    _tapGestureRecognize.numberOfTapsRequired = 1;
    _tapGestureRecognize.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:_tapGestureRecognize];
    
    self.dataTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

#pragma mark - View Events
-(IBAction)cancelClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UISearchBar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [_filteredTableData removeAllObjects];
    
    if(searchText.length > 0)
    {
        [self.dataArray enumerateObjectsUsingBlock:^(NSString* dataItem, NSUInteger index, BOOL* stop){
            if([dataItem rangeOfString:searchText options:NSCaseInsensitiveSearch].location == 0)
            {
                [_filteredTableData addObject:dataItem];
            }
            
        }];
    }
    else
    {
        [_filteredTableData addObjectsFromArray:self.dataArray];
    }
    [self.dataTable reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.tableSearchBar resignFirstResponder];
}

#pragma mark - UITableView delegate methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filteredTableData.count;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.tableHeading;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"genericTableCell"];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:@"genericTableCell"];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"505050"];
    
    if(self.selectedValue)
    {
        cell.accessoryType = ([self.selectedValue isEqualToString:[_filteredTableData objectAtIndex:[indexPath row]]]) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = [_filteredTableData objectAtIndex:[indexPath row]];
    cell.userInteractionEnabled = !self.readOnly;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]init];
    
    NSString* selectedValue = [_filteredTableData objectAtIndex:[indexPath row]];
    __block NSInteger selectedIndex;
    
    [_filteredTableData enumerateObjectsUsingBlock:^(NSString* dataItem, NSUInteger index, BOOL* stop){
        if([dataItem isEqualToString:selectedValue])
        {
            selectedIndex = index;
            *stop = YES;
        }
    }];
    
    [userInfo setObject:selectedValue forKey:@"selectedValue"];
    [userInfo setObject:[NSNumber numberWithInteger:selectedIndex] forKey:@"selectedIndex"];
    
    [self closeView:^(BOOL finished)
     {
         if(finished)
         {
             __weak __typeof__(self) weakSelf = self;
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[NSNotificationCenter defaultCenter]postNotificationName:weakSelf.selectedNotificationName object:weakSelf userInfo:userInfo];
             });
             
         }
     }];
}

-(void)closeView:(myCompletion)compBlock
{
    [self.navigationController popViewControllerAnimated:YES];
    compBlock(YES);
}

#pragma mark - UIScrollView delegate methods
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.tableSearchBar resignFirstResponder];
}

#pragma mark - General
-(void)setupView
{
    self.dataTable.backgroundColor = [UIColor whiteColor];
    _filteredTableData = [[NSMutableArray alloc]init];
    [_filteredTableData addObjectsFromArray:self.dataArray];
    self.navigationItem.title = self.viewTitle;
    
    __block NSInteger selectedRow = -1;
    if(self.selectedValue)
    {
        [self.dataArray enumerateObjectsUsingBlock:^(NSString* dataItem, NSUInteger idx, BOOL* stop){
            if([dataItem isEqualToString:self.selectedValue])
            {
                selectedRow = idx;
                *stop = YES;
            }
        }];
    }
    
    [self.dataTable reloadData];
    
    if(selectedRow > -1)
    {
        [self.dataTable scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:selectedRow inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
}

-(void)dismissKeyboard
{
    [self.tableSearchBar resignFirstResponder];
}

@end