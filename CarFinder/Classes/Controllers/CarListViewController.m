//
//  CarListViewController.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "CarListViewController.h"
#import "GenericSelectionViewController.h"
#import "CarDetailViewController.h"
#import "Car.h"

@interface CarListViewController ()
{
    Cars* _carsModel;
    Car* _newCarModel;
}
@end

@implementation CarListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - View Events
-(IBAction)unwindToCarListView:(UIStoryboardSegue*)segue
{
    [_carsModel getAvailableCars];
}

-(IBAction)resetClicked:(id)sender
{
    _carsModel.selectedYear = 0;
    [self resetView];
    
    [_carsModel clearCarList];
    [self.carListTable reloadData];
}

-(IBAction)addClicked:(id)sender
{
    if(!_newCarModel)
    {
        _newCarModel = [[Car alloc]init];
    }
    else
    {
        [_newCarModel reset];
    }
    [self performSegueWithIdentifier:@"addCarDetailSegue" sender:self];
}

-(IBAction)yearClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(carYearSelected:) name:@"carYearSelected" object:nil];
    
    [self showGenericSelectionViewWithTitle:@"Select Year" tableHeading:@"Year" dataArray:[_carsModel getAvailableYears] selectedValue:[NSString stringWithFormat:@"%ld", (long)_carsModel.selectedYear] isReadOnly:NO selectedNotificationName:@"carYearSelected"];
}

-(IBAction)makeClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(carMakeSelected:) name:@"carMakeSelected" object:nil];
    
    [self showGenericSelectionViewWithTitle:@"Select Make" tableHeading:@"Make" dataArray:[_carsModel getAvailableMakes] selectedValue:_carsModel.selectedMake isReadOnly:NO selectedNotificationName:@"carMakeSelected"];
}

#pragma mark - Notifications
-(void)carYearSelected:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"carYearSelected" object:nil];
    NSDictionary* userInfo = [notification userInfo];
    
    if([[userInfo allKeys]containsObject:@"selectedValue"])
    {
        [self.makeButton setTitle:@"Make" forState:UIControlStateNormal];
        self.makeButton.hidden = NO;
        
        _carsModel.selectedYear = [[userInfo objectForKey:@"selectedValue"]integerValue];
        
        [self.yearButton setTitle:[NSString stringWithFormat:@"%ld", (long)_carsModel.selectedYear] forState:UIControlStateNormal];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"carModelCacheExpired" object:nil];
        
        [_carsModel clearCarList];
        [self.carListTable reloadData];
    }
    else
    {
        //TODO: error handling, invalid notification response
    }
}

-(void)carMakeSelected:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"carMakeSelected" object:nil];
    NSDictionary* userInfo = [notification userInfo];
    
    if([[userInfo allKeys]containsObject:@"selectedValue"])
    {
        _carsModel.selectedMake = [userInfo objectForKey:@"selectedValue"];
    }
    
    [self.makeButton setTitle:_carsModel.selectedMake forState:UIControlStateNormal];
//    [_carsModel getAvailableCars];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"addCarDetailSegue"])
    {
        CarDetailViewController* viewController = (CarDetailViewController*)segue.destinationViewController;
        viewController.carModel = _newCarModel;
    }
    else if ([segue.identifier isEqualToString:@"editCarDetailSegue"])
    {
        CarDetailViewController* viewController = (CarDetailViewController*)segue.destinationViewController;
        viewController.carModel = [_carsModel.carList objectAtIndex:[[self.carListTable indexPathForSelectedRow]row]];
    }
}
#pragma mark - UITableView delegate methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _carsModel.carList.count;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (_carsModel.carList.count > 0) ? @"Available Cars" : nil;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"carCell" forIndexPath:indexPath];
    
    Car* car = [_carsModel.carList objectAtIndexedSubscript:[indexPath row]];
    
    UILabel* label = (UILabel*)[cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%ld", (long)car.year];
    
    label = (UILabel*)[cell viewWithTag:2];
    label.text = car.make;
    
    label = (UILabel*)[cell viewWithTag:3];
    label.text = car.model;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"editCarDetailSegue" sender:self];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        [_carsModel deleteCarAtIndex:[indexPath row]];
        [self.carListTable reloadData];
    }
}

#pragma mark - Cars Delegate Methods
-(void)carModelDataLoadCompleted:(Cars *)carModel
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.carListTable reloadData];
    });
}

#pragma mark - General
-(void)setupView
{
    [super setupView];
    _carsModel = [[Cars alloc]init];
    _carsModel.delegate = self;
    self.makeButton.hidden = YES;
    
    self.carListTable.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"12335D"];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(void)resetView
{
    [self.yearButton setTitle:@"Year" forState:UIControlStateNormal];
    [self.makeButton setTitle:@"Make" forState:UIControlStateNormal];
    self.makeButton.hidden = YES;
}

@end