//
//  TCViewController.m
//  CarFinder
//
//  Created by Andrew Hefele on 7/21/15.
//  Copyright (c) 2015 Andrew Hefele. All rights reserved.
//

#import "TCViewController.h"
#import "GenericSelectionViewController.h"
@interface TCViewController ()

@end

@implementation TCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - General
-(void)showGenericSelectionViewWithTitle:(NSString*)title tableHeading:(NSString*)tableHeading dataArray:(NSArray*)dataArray selectedValue:(NSString*)selectedValue isReadOnly:(BOOL)readOnly selectedNotificationName:(NSString*)selectedNotificationName
{
    UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GenericSelectionViewController* genericView = [mainStoryboard instantiateViewControllerWithIdentifier:@"GenericTableView"];
    
    genericView.viewTitle = title;
    genericView.tableHeading = tableHeading;
    genericView.dataArray = dataArray;
    genericView.selectedValue = selectedValue;
    genericView.readOnly = readOnly;
    genericView.selectedNotificationName = selectedNotificationName;
    
    [self.navigationController pushViewController:genericView animated:YES];
}

-(void)setupView
{

}

@end
